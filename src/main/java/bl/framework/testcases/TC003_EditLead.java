package bl.framework.testcases;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;


public class TC003_EditLead extends SeleniumBase{
	//@Test(dependsOnMethods="bl.framework.testcases.TC002_CreateLead.CLead")
	@Test(groups="smoke")
	public void editLead() 
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		//click on crm/sfa link
		
		/*WebElement eleCRMSFALink = locateElement("xpath", "//a[contains(text(), 'CRM/SFA')]");
		click(eleCRMSFALink);
		
		//click on leads in menu bar
		WebElement eleLeadsLink = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeadsLink);
		
		//Find Leads
		//click on Find leads in menu bar
				WebElement eleFindLeadsLink = locateElement("xpath", "//a[text()='Find Leads']");
				click(eleFindLeadsLink);
				
				//Enter Lead id
				WebElement eleIDTxt = locateElement("xpath", "//input[@name='id']");
				clearAndType(eleIDTxt, "10447");
				
				//Click on Find leads
				WebElement eleFindLeadsBtn = locateElement("xpath", "//button[text()='Find Leads']");
				click(eleFindLeadsBtn);
				
				Thread.sleep(2000);
				//in result table - select first matching record
				WebElement eleTableName = locateElement("xpath", "(//table[@class='x-grid3-row-table'])[1]");
				  List<WebElement> rows = eleTableName.findElements(By.tagName("tr"));
				 WebElement firstrow = rows.get(0);
			 List<WebElement> columns = firstrow.findElements(By.tagName("td"));
				 String firstMatchingLeadid=columns.get(0).getText();
				 
				 WebElement eleFirstMatchingLeadId = locateElement("linktext",firstMatchingLeadid );
				 click(eleFirstMatchingLeadId);
				 
				 
				 
boolean verifyTitle = verifyTitle("View Lead");
				 
				 if(verifyTitle)
					 System.out.println("given value  View Lead Title is matching");
				 else
					 System.out.println("Given value is not matching with the value");
				 
				 //Click on Edit menu/button at the top
				 
				 //Click on Edit lead - 
				 WebElement eleEditLink = locateElement("xpath", "//a[text()='Edit']");
				 click(eleEditLink);
				 Thread.sleep(2000);
				 
				 //Change company name

					
					WebElement eleCompanyNameTxt = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
					clearAndType(eleCompanyNameTxt, "ASP Labs Global Ltd");
					//Click on update button
					WebElement eleUpdateBtn = locateElement("xpath","//input[@value='Update']");
					 click(eleUpdateBtn);
					 
					 //Verify the company name after updation - 
					 
					 WebElement eleCompanyNameLbl = locateElement("xpath","//span[@id='viewLead_companyName_sp']");
					 String getCompanyNametxt = getElementText(eleCompanyNameLbl);
					 
					 if(getCompanyNametxt.contains("ASP Labs Global Ltd"))
					 {
						 System.out.println("Company name "+getCompanyNametxt+"is updated succesfully");
				     }
					 else
						 System.out.println("Company name is not updated ");
		
	}

	private String getElementText(WebElement eleCompanyNameLbl) {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean verifyTitle(String string) {
		// TODO Auto-generated method stub
		return false;
	}

	private void click(WebElement eleLogin) {
		// TODO Auto-generated method stub
		
	}

	private WebElement locateElement(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	private void clearAndType(WebElement eleUsername, String string) {
		// TODO Auto-generated method stub
		
	}

	private void startApp(String string, String string2) {
		// TODO Auto-generated method stub
		*/
	}
}
