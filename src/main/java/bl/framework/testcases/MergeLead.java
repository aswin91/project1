package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;


public class MergeLead extends SeleniumBase {
	
	@Test(groups="sanity")
	public void MLead()  {
		//open a browser
		startApp("chrome", "http://leaftaps.com/opentaps");
		//enter username
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		//enter password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//click login
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//click crm/sfa
		WebElement elink = locateElement("link", "CRM/SFA");
		click(elink);
	
 
	}

}
