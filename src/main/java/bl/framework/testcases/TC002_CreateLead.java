package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;



public class TC002_CreateLead extends SeleniumBase {
	
//	@Test(invocationCount=2,threadPoolSize=2)
	@Test(groups="smoke")
	public void CLead()  {
		//open a browser
		startApp("chrome", "http://leaftaps.com/opentaps");
		//enter username
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		//enter password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//click login
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//click crm/sfa
		WebElement elink = locateElement("link", "CRM/SFA");
		click(elink);
		
		//click create lead
		WebElement elink1 = locateElement("link", "Create Lead");
		click(elink1);
		//enter mandatory details
		WebElement eCompName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eCompName, "Infosys");
		WebElement eFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eFirstName, "Aswin");
		WebElement eLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eLastName, "Kumar");
		WebElement esubmit = locateElement("class", "smallSubmit");
		click(esubmit);
		//to select value from dropdown
	//	WebElement eSource = locateElement("name", "dataSourceId");
		//selectDropDownUsingValue(eSource,"LEAD_PARTNER");
		
		//select class using value
			//	WebElement mark = driver.findElementById("createLeadForm_marketingCampaignId");
				//selectDropDownUsingText(mark, "Catalog Generating Marketing Campaigns");
				//ENTER DOB
				/*WebElement eleDOB = locateElement("name","birthDate");
				clearAndType(eleDOB, "19/02/91");
				//enter email
				WebElement eleEmailTxt = locateElement("id", "createLeadForm_primaryEmail");
				clearAndType(eleEmailTxt, "Test121@gmail.com");
				//click on submit button
				WebElement elesubmit = locateElement("name", "submitButton");
				click(elesubmit);
				
				//verifying lead creaion
				//viewLead_firstName_sp
				WebElement eleFirstNameLabel = locateElement("id",  "viewLead_firstName_sp");
				WebElement eleLastNameLabel = locateElement("id", "viewLead_lastName_sp");
				String txtFstName = getElementText(eleLastNameLabel);
				String txtLstName = getElementText(eleFirstNameLabel);
				if ((txtFstName.equals("Mattew")) && (txtLstName.equals("Booker")))
				{
						System.out.println("lead is created successfully");
				}else 
					System.out.println("lead is not created");*/
		
	}
	

}
