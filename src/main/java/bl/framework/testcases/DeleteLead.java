package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class DeleteLead extends SeleniumBase{
	
	@Test
	@Parameters({"url","uname","upass"})
	//@Test(groups="sanity")
	public void DLead(String url, String uname, String upasss)  {
		//open a browser
		startApp("chrome", url);
		//enter username
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		//enter password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, upasss);
		//click login
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//click crm/sfa
		WebElement elink = locateElement("link", "CRM/SFA");
		click(elink);
	

}
}
